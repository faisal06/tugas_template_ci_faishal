<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Article extends CI_Controller {
    public function list_1()
    {
        $this->load->view('article/a1');
    }
    public function list_2()
    {
        $this->load->view('article/a2');
    }
    public function list_3()
    {
        $this->load->view('article/a3');
    }
}