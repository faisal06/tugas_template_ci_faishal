<!DOCTYPE html>
<html>
<head>
  <title>Beranda | About Us</title>
  <?php $this->load->view('beranda/style');?>
  <?php $this->load->view('article/style');?>
</head>
<body>   
    <?php $this->load->view('layout/header');?>
    <!-- batas header -->
    <article>
    <?php $this->load->view('layout/sidebar');?>    
    <div class = "index">
      <a href="<?=site_url("Article/list_1")?>" title="">About Us</a>
      <img src="<?php echo base_url('assets/image/about.png'); ?>" alt="" class="imga1">
      <small>
            <u>Published By Author On 04 September 2018</u>
      </small>
            <p>Kami adalah perusahaan yang bergerak di bidang teknologi informasi dan telekomunikasi. Seiring dengan
             perkembangan jaman diiringi dengan perubahan dan kemajuan teknologi informasi dan telekomunikasi yang
             begitu cepat, hingga tahun 2016 ini kami memiliki 5 divisi/bidang usaha yang kami sesuaikan dengan 
             perkembangan dan kebutuhan terkini. PT. Cendana Teknika Utama, mempunyai kemampuan managerial, keahlia dan keuangan yang baik, sehingga mampu menjadi perusahaan yang terus berkembang untuk menghasilkan 
             produk dan jasa layanan terbaik bagi penggunanya.</p>
    </div>
    <?php $this->load->view('layout/comment');?>
  </article>
<!-- batas isi -->
    <?php $this->load->view('layout/footer');?>
</body>

</html>