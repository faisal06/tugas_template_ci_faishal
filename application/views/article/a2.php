<!DOCTYPE html>
<html>
<head>
  <title>Beranda | Company Overview</title>
  <?php $this->load->view('beranda/style');?>
  <?php $this->load->view('article/style');?>
</head>
<body>   
<?php $this->load->view('layout/header');?>
    <!-- batas header -->
  
    <article>
    <?php $this->load->view('layout/sidebar');?>

    <div class = "index">
      <a href="<?=site_url("Article/list_2")?>" title="">Company Overview</a>
      <img src="<?php echo base_url('assets/image/Overview1.png'); ?>" alt="" class="imga2">
      <small>
        <u>Published By Author On 08 September 2018</u>
      </small>
            <p>
             &emsp; &emsp;CENDANA TEKNIKA UTAMA atau lebih dikenal dengan nama CENDANA2000 berdiri pada tahun 1998 di
             Jawa Timur. Perusahaan didirikan oleh 3 (tiga) orang, yaitu Marsutiyawan Aji, Muhammad Rifai dan Imam Masyhuri.
             CENDANA TEKNIKA UTAMA memiliki kantor pusat di Kota Malang dengan alamat Ruko Permata Griya Shanta NR 24-25,Jalan Soekarno Hatta, Kel. Jatimulya, Kec. Lowokwaru, Kota Malang. Kantor cabang Jakarta beralamat di Jl.Kebagusan Raya no 192, Pasar Minggu, Jakarta Selatan. CENDANA TEKNIKA UTAMA didukung oleh tenaga professional yang pengalaman lebih dari 15 tahun. Pendiri perusahaan mempunyai keinginan yang kuat untuk berperan secara aktif dalam pembangunan Indonesia. Inovasi adalah kata kunci untuk mengembangkan perusahaan. Dengan kekuatan budaya kerja yang dimilikinya, perusahaan berkomitmen untuk memberikan pelayanan terbaik dan pelayanan purnah di jual yang memuaskan pelanggan. CENDANA TEKNIKA UTAMA mengalami restrukturisasi perusahaan pada tahun 2006, sehingga komposisi kepemilikan berubah dari kepemilikan perorangan menjadi kepemilikan badan usaha dimana PT.KARYATAMA SOLUSINDO mempunyai komposisi saham sebesar 70% dan PT. CATUR ELANG PERKASA sebesar 30%. Restrukturisasi ini dilakukan dalam rangka mengakselerasi perkembangan perusahaan.
          </p>
          <a>Struktur Organisasi</a>
          <img src="<?php echo base_url('assets/image/struktur.png'); ?>" alt="" class="img1a2">
    </div>
    <?php $this->load->view('layout/comment');?>
  </article>
<!-- batas isi -->
    <?php $this->load->view('layout/footer');?>
</body>

</html>