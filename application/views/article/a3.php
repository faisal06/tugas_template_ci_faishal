<!DOCTYPE html>
<html>
<head>
  <title>Beranda Website Cendana</title>
  <?php $this->load->view('beranda/style');?>
  <?php $this->load->view('article/style');?>
</head>
<body>   
<?php $this->load->view('layout/header');?>
    <!-- batas header -->
  
    <article>
    <?php $this->load->view('layout/sidebar');?>
    
    <div class = "index">
      <a href="<?=site_url("Article/list_3")?>" title="">Divisi</a>
      <img src="<?php echo base_url('assets/image/divisi.png'); ?>" alt="" class="imga3">
      <small>
          <u>Published By Author On 10 September 2018</u>
      </small>
            <p>
             &emsp; &emsp;Divisi IT Multimedia merupakan divisi atau bidang usaha kami yang fokus di bidang multimedia yaitu berbagai alat digunakan sebagai media penyampaian informasi yang di dalamnya terdapat perpaduan (kombinasi) berbagai bentuk elemen informasi, seperti teks, graphics, animasi, video, interaktif maupun suara. Dengan tenaga-tenaga professional yang ada, divisi kami memproduksi dan menjual alat-alat tersebut yang dikembangkan sesuai dengan perkembangan teknologi terkini.
             <br>
          </p>

          <a href="" title="" class="small">Adapun alat-alat tersebut terdiri dari kategori :  </a>
             
          <div class="mid">
            <a class="small">ALAT ANTRIAN</a>
            <p>
             Alat untuk menyampaikan informasi nomer antrian 
             kepada konsumen.Jenis-jenis alat antrian yang 
             kami tawarkan meliputi: mesin antrian android, 
             mesin antrian touchscreen mesin antrian microq,
             mesin antrian led running text, dan mesin antrian minimalis.
             </p>
        </td>
          </div>
          <div class="mid">
            <a class="small">SERBAMULTIMEDIA</a>
            <p>
              Alat untuk menyampaikan informasi maupun mengiklan
              kan/mempromosikan produk kepada masyarakat atau 
              konsumen. Jenis produk serba multimedia yang kami tawarkan meliputi 
              android, digital signage,LG ezsign, imedia, dan kiosk interaktif.
            </p>
          </div>
          <div class="mid">
            <a class="small">ALAT SKP</a>
            <p>
              Survey kepuasan pelanggan/ Indeks kepuasan masyarakat yaitu alat untuk mengetahui
              dan menganalisa data survey kepuasan pelanggan terhadap produk, jasa, maupun fasilitas
              dan pelayanaan suatu instansi. Tersedia report atau laporan yang up to date guna analisa 
              data kepuasan pelanggan.
            </p>
          </div>
    </div>
    <?php $this->load->view('layout/comment');?>

  </article>
<!-- batas isi -->
<?php $this->load->view('layout/footer');?>
</body>

</html>