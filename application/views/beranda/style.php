<style>
            body{
            background : #f1f1f1;
            font-family: sagoe ui;
          }
            .after_img{
          height: 300px;
          max-width: 101%;
          margin-left: -7px;
          margin-right: -10px;
          background: url(<?php echo base_url('assets/image/body.jpg'); ?>);
          filter: blur(2px);
          padding-top: 350px;
          margin-bottom: 150px;
        }

        .after_text{
          position: absolute;
          margin-left: 170px;
          margin-top: -500px;
        }
        .after_text a:first-child{
          
          font-size: 70px;
          text-decoration: none;
          /*background-image: linear-gradient(147deg, #166d3b 0%, #000000 74%);*/
          color: transparent;
          border-top-left-radius: 15px 15px;
          border-bottom-left-radius: 15px 15px;

        }
        .after_text a{
          margin-left: 0px;
          font-size: 70px;
          text-decoration: none;
          color: transparent;
        }
        header1:hover .after_text {
          transition: 2s ease;
          margin-left: 170px;
          font-size: 70px;
          background-image: linear-gradient(147deg, #166d3b 0%, #000000 74%);
          border-top-left-radius: 15px 15px;
          border-bottom-left-radius: 15px 15px;
          border-top-right-radius: 15px 15px;
          border-bottom-right-radius: 15px 15px;
          font-color: white;
          padding-left:5px;
          padding-right:5px;

        }
        header1:hover .after_text a{
          transition: 2s ease;
          color: white;
        }
        header1:hover .after_text a:last-child{
          transition: 3s ease;
          color:#f48642;
        }


        .after_search{
          position: absolute;
          margin-top: -700px;
          margin-left: 75%;
          color: white;
          opacity:0.7;
        }
        header1:hover .after_search{
          opacity:1;
        }

        article a{
          color : #384c37;
          text-decoration: none;
          font-weight: bold;
          cursor: pointer;
        }
        article a:hover{
          text-shadow: 2px 5px 10px #446042;
        }

        .list * {margin: 0 ; padding: 0;}
        
        .list {
          margin: 20px;
          margin-left: 100px;
        }
        
        .list ul {
          list-style-type: none;
          width: 700px
        }
        
        .list h3  {
          margin-top: 50px;
          margin-bottom: 20px;
          font-size: 30px;
        }
        
        .list li img {
          float: left;
          margin: 0 15px 0 0;
          width: 200px;
          height: 230px;

        }
        
        .list li p {
          font: 200 12px/1.5 Georgia, Times New Roman, serif;
        }
        
        .list li {
          padding: 10px;
          display: inline-block;
          background: #ffffff;
          margin-bottom: 30px;
          border-radius: 7px;
          box-shadow: 2px 2px 10px #888888 ;
        }
        

        .list li:hover{
          box-shadow: 3px 5px 10px #888888 ;
        }
  </style>