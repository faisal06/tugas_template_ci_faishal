<!DOCTYPE html>
<html>
<head>
	<title>Beranda Website Cendana</title>
  <?php $this->load->view('beranda/style');?>

</head>
<body>	 
  <?php $this->load->view('layout/header');?>
    <header1>
      <div class="after_img">
        <!-- <img src="image/body.jpg" alt=""> -->
      </div>
      <div class="after_text">
        <a href=""> CENDANA</a>
        <a href=""> 2000</a>
      </div>
      <div class="after_search">
        <label> Search : 
          <input type="search" name="" >
        </label>
      </div>
    </header1>
    <article>
      <?php $this->load->view('layout/sidebar');?>
    <div class = "list">
      <ul>
        <li>
          <img src="<?php echo base_url('assets/image/about.png'); ?>" />
          <small></small>
          <h3> <a href="<?=site_url("Article/list_1")?>" title=""> About Cendana </a></h3>
          <p> Kami adalah perusahaan yang bergerak di bidang teknologi informasi dan telekomunikasi.
            Seiring dengan perkembangan jaman diiringi dengan perubahan dan kemajuan teknologi
            informasi dan telekomunikasi yang begitu cepat, hingga ... 
            <a href="<?=site_url("Article/list_1")?>"><strong>Baca Selengkapnya</strong></a>
            <small><br>
              <u>Published By Author On 04 September 2018</u>
            </small>
          </p>
        </li>
          
        <li>
          <img src="<?php echo base_url('assets/image/overview.png'); ?>" />
          <h3> <a href="<?=site_url("Article/list_2")?>" title=""> Company Overview </a></h3>
          <p> CENDANA TEKNIKA UTAMA atau lebih dikenal dengan nama CENDANA2000 berdiri
              pada tahun 1998 di Malang, Jawa Timur. Perusahaan didirikan oleh 3 (tiga)orang,
              yaitu Marsutiyawan Aji,Muhammad Rifai dan Imam Masyhuri.CENDANA TEKNIKA
              UTAMA memiliki kantor pusat .... 
              <a href="<?=site_url("Article/list_2")?>">
              <strong>Baca Selengkapnya</strong>
            </a><br>
            <small>
              <u>Published By Author On 08 September 2018</u>
            </small>
          </p>
        </li>
     
        <li>
          <img src="<?php echo base_url('assets/image/division.png'); ?>" />
          <h3> <a href="<?=site_url("Article/list_3")?>" title=""> Divisi </a></h3>
          <p>Divisi IT Multimedia merupakan divisi atau bidang usaha kami yang 
            fokus di bidang multimedia, yaitu berbagai alat yang digunakan sebagai
            media penyampaian informasi yang di dalamnya terdapat perpaduan (kombinasi)
            berbagai bentuk elemen informasi, seperti teks, graphics....
            <a href="<?=site_url("Article/list_3")?>">
              <strong>Baca Selengkapnya</strong>
            </a><br>
            <small>
              <u>Published By Author On 10 September 2018</u>
            </small>
          </p>
        </li>
     
      </ul>
    </div>


  </article>
<!-- batas isi -->
  <?php $this->load->view('layout/footer');?>
</body>

</html>