<style>
    .rightbar{
      width: 25%;
      float: right;
      margin-top:100px;
    }
    .rightbar .category{
      background : #ffffff;
      box-shadow: 2px 2px 10px #888888 ;
      border-radius: 8px;
      padding-top: 1px;
      margin-bottom: 50px;
    }
    .rightbar h3{
      display: block;
      color: #ffffff;
      background-image: linear-gradient(to right, #324731 50%,#ffffff 50%);
      border-bottom: solid #324731;
      padding-left: 20px;
    }
    .rightbar .category a{
      display: block;
      text-decoration: none;
      padding-top: 10px;
      padding-bottom: 10px;
      padding-left: 30px;
      color: #324731;
      font-size : 17px;
    }
    .rightbar .category a:hover{
      transition: 0.5s;
      background :#324731;
      color: white;
    }
    .rightbar .category:hover{
      box-shadow: 3px 5px 10px #888888 ;
    }

    .rightbar .populer{
      background : #ffffff;
      box-shadow: 2px 2px 10px #888888 ;
      border-radius: 8px;
      padding-top: 1px;
      padding-bottom: 20px;
      height: auto;
    }
    .rightbar .populer a{
      font-size: 16px;
      margin-bottom: -10px;
      text-decoration: none;
    }
    .rightbar .populer li{
      display: block;
      margin-left: -40px;
      padding-bottom: 10px;
      padding-left: 10px;
    }
    .rightbar .populer li img {
      float: left;
      width: 70px;
      height: 80px;
      margin-right: 10px;
      padding-left: 10px;
    }
    .rightbar .populer:hover{
      box-shadow: 3px 5px 10px #888888 ;
    }
</style>
<div class="rightbar">
      <div class="category">
        <h3>List Category</h3>
          <a href="" title="">Product Kami</a>
          <a href="" title="">Referensi</a>
          <a href="" title="">Alamat Cabang</a>
          <a href="" title="">Lowongan</a>
      </div>
      <div class="populer">    
      <h3>Populer Post</h3>
      <ul>
        <li>
          <img src="<?php echo base_url('assets/image/about.png'); ?>" />
          <a href="<?=site_url("Article/list_1")?>" title="">About Cendana</a>
          <p>Kami adalah perusahaan yang bergerak di bidang teknologi...</p>
        </li>
         <li>
          <img src="<?php echo base_url('assets/image/overview.png'); ?>" />
          <a href="<?=site_url("Article/list_2")?>" title="">Company Overview</a>
          <p>CENDANA TEKNIKA UTAMAatau lebih dikenal dengan....</p>
        </li>
        <li>
          <img src="<?php echo base_url('assets/image/division.png'); ?>" />
          <a href="<?=site_url("Article/list_3")?>" title="">Divisi</a>
          <p>Divisi IT Multimedia merupakan....</p>
        </li>
      </ul>
      </div>  
    </div>